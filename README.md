These are the programming assignments relative to Pattern Oriented Software Architecture (POSA):

* Assignment 0: understanding how to (1) create, (2) start, (3) interrupt, and (4) wait for the completion of multiple Java Threads
* Assignment 1: understanding how to use ReentrantReadWriteLock to serialize access to a variable that's shared by multiple threads
* Assignment 2: understanding hot to use the SimpleSemaphore to correctly implement a resource manager that limits the number of Beings from Middle-Earth who can concurrently gaze into a Palantir
* Assignment 3: implementing a Java program that creates two instances of the PlayPingPongThread and then starts these thread instances to correctly alternate printing "Ping" and "Pong", respectively, on the console display
* Assignment 4: implementing a program that uses a pattern-oriented ping-pong framework to spawn threads that correctly alternate printing "Ping" and "Pong", respectively on either the command-line display
* Assignment 5: implementing a program that downloads images using started services and then displays them in the context of the UI Thread.
* Assignment 6: reviewing an Android application that contains potential security vulnerabilities and/or code abstractions that do not exhibit secure abstraction design best practices
* Assignment 7: implementing a program that downloads images using Bound Services and then displays them in the context of the UI Thread

For more information, I invite you to have a look at https://www.coursera.org/course/posa